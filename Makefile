# todo : put outputs into build subdir

all: seed-x86-linux seed-x86-bios

.PHONY: clean clean-seed run-x86-linux

clean:
	rm -rf ./build

clean-seed: clean
	rm -rf ./seeds

.SECONDARY: %.o
.PRECIOUS: %.o
.PRECIOUS: %.bb
.PRECIOUS: %.mb
.PRECIOUS: %.eb
.PRECIOUS: %.mb-dbg
.PRECIOUS: build/arch-x86-bios/2wf-bios.rev-octal build/arch-x86-linux/2wf.mb build/arch-x86-linux/monitor-octal-x86-linux.o build/arch-x86-bios/2wf-bios.mb build/arch-x86-linux/monitor-octal-x86-linux.eb build/arch-x86-bios/2wf-bios.bb build/arch-x86-linux/2wf.o build/arch-x86-linux/2wf.eb

seed-x86-bios: seeds/arch-x86-bios/monitor-octal-x86-kbd.bb seeds/arch-x86/2wf.m16.rev-octal
seed-x86-bios: seeds/arch-x86/2wf.m16 seeds/arch-x86/2wf.s16
seed-x86-bios: seeds/2wf/2wf-init0.fw Makefile

seed-x86-linux: seeds/arch-x86-linux/monitor-octal-x86-linux.eb seeds/arch-x86/2wf.m32.rev-octal
seed-x86-linux: seeds/arch-x86/2wf.s32
seed-x86-linux: seeds/2wf/2wf-init0.fw Makefile
#seed-x86-linux: seeds/2wf/2wf-init1.fs

#########################################################################################
build/arch-x86/2wf.m16: src/arch-x86-linux/2wf.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D bios16=1 -D monitorBinary=1 -f bin -o $@ -l $@.lst $< -m x86
	yasm  -Worphan-labels -D bios16=1 -D monitorBinary=1 -f elf -g dwarf2 -o $@-dbg $< -m x86

build/arch-x86/2wf.s16: src/arch-x86-linux/2wf.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D bios16=1 -U monitorBinary -f bin -o $@ -l $@.lst $< -m x86
	yasm  -Worphan-labels -D bios16=1 -U monitorBinary -f elf -g dwarf2 -o $@-dbg $< -m x86

# StartTextLinuxAfterMonitor = 0x08048091
StartT = 0x08048091
build/arch-x86/2wf.m32: src/arch-x86-linux/2wf.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D linux32=1 -D monitorBinary=1 -f elf -g dwarf2 -o $@-dbg  $< -l $@.lst -m x86
	ld -m elf_i386 --omagic --no-dynamic-linker -static -o $@-bin -Ttext $(StartT) $@-dbg
	objcopy -v --only-section=.text --output-target binary  --image-base $(StartT) $@-bin $@
# objdump -b binary -m i386 -hD build/2wf-x86-linux/2wf-m.mb | less
# hexdump -C build/2wf-x86-linux/2wf-m.mb
# gdb: break 62; condition 2 $edi == 0x8048099; set *(char)0x8048091 = 0x00

# LEN=$$(wc -c  < build/arch-x86-linux/monitor-octal-x86.mb); BASE=$$(printf "0x%x" $$(( 0x8048090 + $$LEN - 1)) # 0x8048060 + sizeof monitor - 1

build/arch-x86/2wf.s32: src/arch-x86-linux/2wf.asm Makefile build/tools/set-elf-text-writeable
	mkdir -p $(@D)
	yasm -Worphan-labels -D linux32=1 -U monitorBinary -f elf -g dwarf2 -o $@.o -l $@.lst $< -m x86
	ld --omagic -nostdlib -no-pie -m elf_i386 --no-dynamic-linker -static -o $@.tmp $@.o
	objcopy --writable-text --set-section-flags .text=CONTENTS,ALLOC,LOAD,CODE $@.tmp $@.tmp2
	./build/tools/set-elf-text-writeable $@.tmp2
	mv $@.tmp2 $@
	rm $@.tmp
# objcopy --writable-text does not work for elf ...
#########################################################################################

build/test.inp: build/arch-x86-linux/2wf.rev-octal build/2wf/2wf-init0.fw Makefile
	( cat build/arch-x86-linux/2wf.rev-octal ; head -10 build/2wf/2wf-init0.fw ) > build/test.inp

build/%.o-gas: src/%.s Makefile
	mkdir -p $(@D)
	as -a=$(@:.o-gas=.lst) -defsym linux=1 -R -march=i386 --32 -o $@ $<

build/%.o: src/%.asm Makefile
	mkdir -p $(@D)
	yasm -Worphan-labels -D standalone=1 -U monitorBinary -f elf -g dwarf2 -o $@ -l $(@:.o=.lst) $< -m x86

 # elf binary i.e. for linux
build/%.eb: build/%.o build/tools/set-elf-text-writeable Makefile
	ld --omagic -nostdlib -no-pie -m elf_i386 --no-dynamic-linker -static -o $@.tmp $<
	objcopy --writable-text --set-section-flags .text=CONTENTS,ALLOC,LOAD,CODE $@.tmp $@.tmp2
	./build/tools/set-elf-text-writeable $@.tmp2
	mv $@.tmp2 $@
	: objcopy --writable-text does not work for elf ...
	rm $@.tmp
# to check if section .text is writable use:
# objdump -hD %.eb | less

build/tools/set-elf-text-writeable: tools/set-elf-text-writeable.c Makefile
	mkdir -p $(@D)
	gcc -o $@ $<

# mb - monitor binary
build/arch-x86-bios/2wf-bios.mb: src/arch-x86-bios/2wf-bios.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D monitorBinary -U standalone -f elf -g dwarf2 -o $@-dbg  $< -l $(@:.mb=.lst) -m x86
	: ld -T src/arch-x86-linux/2wf.mb-ld -m elf_i386 --strip-all --omagic --no-dynamic-linker -static -o $@-bin -Ttext 0x0804891 $@-dbg
	: ld -m elf_i386 --strip-all --omagic --no-dynamic-linker -static -o $@-bin -Ttext 0x7711 $@-dbg
	ld -m elf_i386 --omagic --no-dynamic-linker -static -o $@-bin -Ttext 0x7711 $@-dbg
	objcopy -v --only-section=.text --output-target binary --image-base 0x7711 $@-bin $@

build/arch-x86-linux/2wf.mb: src/arch-x86-linux/2wf.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D monitorBinary -U standalone -f elf -g dwarf2 -o $@-dbg  $< -l $(@:.mb=.lst) -m x86
	: ld -T src/arch-x86-linux/2wf.mb-ld -m elf_i386 --strip-all --omagic --no-dynamic-linker -static -o $@-bin -Ttext 0x08048091 $@-dbg
	: ld -m elf_i386 --strip-all --omagic --no-dynamic-linker -static -o $@-bin -Ttext 0x08048091 $@-dbg
	ld -m elf_i386 --omagic --no-dynamic-linker -static -o $@-bin -Ttext 0x08048091 $@-dbg
	objcopy -v --only-section=.text --output-target binary --image-base 0x08048091 $@-bin $@
# objdump -b binary -m i386 -hD build/2wf-x86-linux/2wf-m.mb | less
# hexdump -C build/2wf-x86-linux/2wf-m.mb
# gdb: break 62; condition 2 $edi == 0x8048099; set *(char)0x8048091 = 0x00

# LEN=$$(wc -c  < build/arch-x86-linux/monitor-octal-x86.mb); BASE=$$(printf "0x%x" $$(( 0x8048090 + $$LEN - 1)) # 0x8048060 + sizeof monitor - 1


# yasm  -Worphan-labels -D monitorBinary -U standalone -f bin -o $@ -l $(@:.mb=.lst) $< -m x86
# ld -m elf_i386 --no-dynamic-linker -static -o $@-dgb-bin -Ttext 0x0804891 $@-dbg
# ld -m elf_i386 --strip-all --omagic --no-dynamic-linker -static --oformat binary -o $@-bin -Ttext 0x0804891 $@-dbg

build/arch-x86-bios/2wf-boot.bb: src/2wf-x86-linux/2wf-boot.asm Makefile # bb - bios binary
	mkdir -p $(@D)
	yasm  -Worphan-labels -D directBoot=1 -U monitorBinary -f bin -o $@ -l $(@:.bb=.lst) $< -m x86
	yasm  -Worphan-labels -D directBoot=1 -U monitorBinary -f elf -g dwarf2 -o $@-dbg $< -m x86

# bb - bios binary
build/arch-x86-bios/monitor-octal-x86-kbd.bb: src/arch-x86-bios/monitor-octal-x86-boot.asm Makefile
	mkdir -p $(@D)
	yasm  -Worphan-labels -D keyboard=1 -U serial -f bin -o $@ -l $(@:.bb=.lst) $< -m x86

build/arch-x86-bios/monitor-octal-x86-serial.bb: src/arch-x86-bios/monitor-octal-x86-boot.asm # bb - bios binary
	mkdir -p $(@D)
	yasm  -Worphan-labels -D serial=1 -U keyboard -f bin -o $@ -l $(@:.bb=.lst) $< -m x86

build/arch-x86-bios/monitor-octal-x86-serialInit.bb: src/arch-x86-bios/monitor-octal-x86-boot.asm # bb - bios binary
	mkdir -p $(@D)
	yasm  -Worphan-labels -D serial=1 -D serialInit=1 -U keyboard -f bin -o $@ -l $(@:.bb=.lst) $< -m x86

build/arch-x86-bios/monitor-octet-x86-kbd.bb: src/arch-x86-bios/monitor-octet-x86-boot.asm # bb - bios binary
	mkdir -p $(@D)
	yasm  -Worphan-labels -D keyboard=1 -U serial -f bin -o $@ -l $(@:.bb=.lst) $< -m x86

build/arch-x86-bios/monitor-octet-x86-serial.bb: src/arch-x86-bios/monitor-octet-x86-boot.asm # bb - bios binary
	mkdir -p $(@D)
	yasm  -Worphan-labels -D serial=1 -U keyboard -f bin -o $@ -l $(@:.bb=.lst) $< -m x86

build/arch-x86-bios/monitor-octet-x86-serialInit.bb: src/arch-x86-bios/monitor-octet-x86-boot.asm # bb - bios binary
	mkdir -p $(@D)
	yasm  -Worphan-labels -D serial=1 -D serialInit=1 -U keyboard -f bin -o $@ -l $(@:.bb=.lst) $< -m x86

build/%.bb: src/%.asm Makefile # bb - bios binary
	mkdir -p $(@D)
	yasm  -Worphan-labels -f bin -o $@ -l $(@:.bb=.lst) $< -m x86
#objdump -hD build/arch-x86-bios/monitor-x86-serial.mb -m i386 -M addr16 -b binary | less

build/%.rev-octal: build/% Makefile
	mkdir -p $(@D)
	python ./tools/reverse-octal.py 512 < $< > $@.tmp
	mv $@.tmp $@

build/%.rev-octet: build/%.mb
	mkdir -p $(@D)
	python ./tools/reverse-octet.py 512 < $< > $@.tmp
	mv $@.tmp $@

seeds/arch-x86-linux/monitor-octal-l86-linux.eb: build/monitor/monitor-octal-x86-linux.eb
	mkdir -p $(@D)
	cp $^ $@

seeds/x86-linux/2wf.rev-%: build/2wf-x86-linux/2wf.rev-%
	mkdir -p $(@D)
	cp $^ $@

seeds/2wf/2wf-init0.fw: build/2wf/2wf-init0.fw
	mkdir -p $(@D)
	cp $^ $@

seeds/%: build/%
	mkdir -p $(@D)
	cp $^ $@

#build/2wf-init0.fw: src/2wf-x86-linux/2wf-init0.fw
#	mkdir -p $(@D)
#	cp $^ $@

build/%.fw: src/%.fs Makefile
	mkdir -p $(@D)
	sed -e 's/\\ .*$$//' -e 's/( [^)]*)//g' -e '/^ *$$/d' -e 's/  */ /g' -e 's/  *$$//' -e 'w$@' $< > /dev/null

.PHONY: clean run-x86-linux

run-x86-linux: seeds/x86-linux/2wf.rev-octal src/2wf/init0.fw src/2wf/init1-x86-linux.fs - > ./seeds/x86-linux/monitor-octal-x86-linux.eb
	cat ./seeds/x86-linux/2wf.rev-octal seeds/2wf/init0.fw src/2wf/init1-x86-linux.fs - > ./seeds/x86-linux/monitor-octal-x86-linux.eb

X%.o: %.s
	as -R -gstabs -32 -o $(^:.s=.o) -L -a=$(^:.s=.lst) $^

X%.bin: %.o
	ld -no-pie --omagic -nostdlib -no-pie -m elf_i386 --no-dynamic-linker -static -o $(^:.o=.bin) $^
# --section-start=sectionname=org
# -Tbss=org  --section-start=bss=org
# -Tdata=org
# -Ttext=org
# -Ttext-segment=org
