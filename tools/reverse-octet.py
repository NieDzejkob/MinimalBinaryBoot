# Copyright (C) 2021 Dr. Stefan Karrmann
# Distribution and usage licenced under GPL-3.
import sys
buffer = bytes(sys.stdin.read())
for i in range(len(buffer), int(sys.argv[1])): # pad end with zeros
    sys.stdout.write(chr(0))
for i in range(len(buffer) - 1, -1, -1):
    sys.stdout.write(buffer[i])
