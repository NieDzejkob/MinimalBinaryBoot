# Copyright (C) 2021 Dr. Stefan Karrmann
# Distributed unter GPL-3 licence.
# 0x30 or 48 bytes of x86 binary code
# Based on https://niedzejkob.p4.team/bootstrap/miniforth/
# comfort extension
# 1. use first two bytes as size
# 2. add line comments for # and ;
# 3. ignore non octal digits, i.e. [^0-7]

.macro jmpb target
  pre_jump_byte_\@ :
  jmp   \target
  post_jump_byte_\@ :
  .set    jump_len_\@, post_jump_byte_\@ - 2 - pre_jump_byte_\@
  .fill   0xffffffff * jump_len_\@, 4, 0xdeadbeef
  # .error "Offset to big for byte jump"
.endm

.equ       bufferSize, 511 # a boot block - Adjust to taste. Beware of fenceposting.

.section .data
message:
.zero  1

# .code16
.code32

.section .text, "wx", @progbits
.global _start
_start:
init:
#  we do not use the stack, linux initializes the segment registers
#  push word 0                   ; hex: 6a00
#  pop es                        ; hex: 07
  std                           # hex: FD
  movl  $buffer+bufferSize, %edi # hex: bfFF010000 ; Adjust to taste. Beware of fenceposting.
output_loop:
  xorl  %esi,%esi               # hex: 31F6
  incl  %esi                    # hex: 46
#  mov   esi,1                   ; hex: BB0100 ; collector of input with ready flag
input_loop:                      # hex:
# mov ah, 0                      ; hex: b400
# int 0x16                       ; hex: cd16

# serial input
#  mov ah,1                      ; hex: b401
#  xor cx,cx                     ; hex: 31c9
#  int 0x14                      ; hex: cd14

  # read(0, message, 1)
  xorl  %ebx,%ebx               # hex: 31DB file handle 0 is stdin
  movl  $message, %ecx          # hes: B9 92 82 04 08 address of string to input
  movl  %ebx,%edx               # hex: 89DA
  incl  %edx                    # hex: 42  1 = number of bytes
  movl  %edx,%eax               # hex: 89D0
  incl  %eax                    # hex: 40
  incl  %eax                    # hex: 40   system call 3 is read
  int   $0x80                   # hex: CD80 invoke operating system to do the write
  # xor   eax,eax               ; we assume return code 1, i.e. one byte read, here
  movb  message,%bl             # hex: a0 92 82 04 08

  subb  $'0', %bl               # hex: 80EB30
  movl  %esi,%eax               # hex: 89F0
  shll  $3,%eax                 # hex: C1E003
  orb   %bl,%al                 # hex: 08D3
  movl  %eax,%esi               # hex: 89D6
  orb   %ah,%ah                 # hex: 08E4
  #cmp   ah, 0x00                ; hex: 66 81FBFF00
  jz    input_loop              # hex: 74D8
  stosb                         # hex: aa
  jmpb   output_loop             # hex: EBD2
  #jmp   short buffer           ; hex EBFE , i.e. FE must be the first byte in the loaded code
                                # as the bytes are reversed, it's actually the last octal code 376
buffer:
.zero   bufferSize
Xmessage:
.zero   1
reservedToBeUsedByLoadedCode:
.zero   0x1000
