; Copyright 2022 Dr. Stefan Karrmann
; Licenced under GPL-3.

; Register usage:
; SP = parameter stack pointer (grows downwards)
; BP = return stack pointer (grows downwards)
; SI = execution pointer
;
; Dictionary structure:
; link: dw
; name: counted string (with flags)
;
; This Forth, i.e. 2wf, is DTC, as this saves a wordsize for each
; defcode, while costing 1 byte for each defword (of which there are
; none in the bootsector).

F_IMMEDIATE equ 0x80
F_HIDDEN    equ 0x40
F_LENMASK   equ 0x3f            ; 0x40 - 1
F_STATE_COMPILE   equ 0
F_STATE_INTERPRET equ 1

CPU i386

%ifdef monitorBinary
  %undef standalone
%else
  %define standalone
%endif

%ifdef bios16
  %undef linux32
  %ifdef serial
    %undef keyboard
  %else
    %define keyboard
  %endif
%else
  %define linux32
%endif

%ifdef linux32
  BITS 32
  %define WordSize 4
  %define EAX  eax
  %define EBX  ebx
  %define ECX  ecx
  %define EDX  edx
  %define ESP  esp
  %define EBP  ebp
  %define ESI  esi
  %define EDI  edi
  %define LODSWS  lodsd
  %define STOSWS  stosd
  %define DWS dd
  %define WSword dword
%endif ; linux32

%ifdef bios16
  BITS 16
  %define WordSize 2
  %define EAX  ax
  %define EBX  bx
  %define ECX  cx
  %define EDX  dx
  %define ESP  sp
  %define EBP  bp
  %define ESI  si
  %define EDI  di
  %define LODSWS  lodsw
  %define STOSWS  stosw
  %define DWS dw
  %define WSword word
%endif ; bios16

PageSize        equ 0x1000 ; 4096 bytes
bufferSize      equ PageSize
hereSize        equ 4 * PageSize
RSsize          equ PageSize

%define NEXT  jmp short next

%define link 0

; defcode ColonLBrack, "|", F_IMMEDIATE
; defcode Comma, ","
%macro defcode 2-3 0
word_%1:
  DWS    link
  %define link    word_%1
  %strlen %%len %2
    db %3 | %%len, %2
%1:
%endmacro

  LinuxStart    equ 0x8048060
  BIOSStart     equ 0x7700      ; in segment 0050, in total 0x7c00
%ifdef bios16
  SizeOfMonitor equ 0x20
  StartMonitor  equ BIOSStart
%else
%ifdef linux32
  SizeOfMonitor equ 0x32
  StartMonitor  equ LinuxStart
global _start

%else
%error "Unknown architecture"
%endif ; linux32
%endif ; bios16
  StartThis     equ StartMonitor + (SizeOfMonitor - 1)

%ifdef monitorBinary
SECTION .text   align=1
%ifdef ORG
ORG StartThis
%endif ; ORG

jumpOffset:                       ; from last istruction of the monitor
  ;db    0x00                     ; jump offset of next instruction to _start
  ;jmp   short _start     ; copy it from this if unsure but comment the next line
  db    WordSize * 13 ; 0x34 where 13 entries are on the startStack
%endif ; monitor Binary

%ifdef standalone
  %ifdef linux32

SECTION .data
  resb  PageSize

  %endif ; linux32
  %ifdef bios16
SECTION .text   write
        %ifdef ORG
  ORG   StartThis
        %endif ; ORG
  jmp   0x0050:_start           ; initialize cs
  %endif ; bios16
%endif ; standalone

startStack:
  DWS    Semi               ; Must be top of stack at start !
  DWS    Fetch
  DWS    Store
  DWS    Substract
  DWS    Nand
  DWS    PSPFetch
  DWS    RSPFetch
  DWS    Key
  DWS    Emit
  DWS    QExit
  DWS    STATE
  DWS    LATEST
  DWS    HERE
;  DWS    TIB
;  DWS    TOIN
%define PSP0 $ - WordSize       ; bottom of parameter stack

%ifdef standalone
SECTION .text   write
%endif ; standalone

_start:
%ifdef bios16
  push  cs
  push  cs
%ifndef monitorBinary
  push  cs
  pop   es                      ; we need es for di in the monitor
%endif ; monitorBinary
  pop   ds
  pop   ss                      ; if ss is set, cli is active for the next instruction:
%endif ; bios16
  mov   ESP, startStack
  cld
%ifdef serialInit
   mov ax, 0x00e3        ; ah = 0xe3 = 0b111000111
   ; i.e. bit2 1 and 0: 8 bit word length
   ;      bit 2       : 1 stop bit
   ;      bits 3 and 4: no parity
   ;      bits 5 to 7: baud rate 9600
   xor dx, dx
   int 0x14
%endif ; serialInit

; stackbottom: ifdef monitorBinary

QUIT:                           ; (re)start the interperter, we do not reset state here
  mov   EBP, RS0
InterpreterLoop:
  call  readWord

; Try to find the word in the dictionary.
; ESI = dictionary pointer
; EDI = string pointer
; ECX = string length
LATEST equ $+1
  mov   ESI, startLatest
.find:
  LODSWS
  push  EAX ; save pointer to next entry
  lodsb
  xor   al, cl ; if the length matches, then AL contains only the flags
  test  al, F_HIDDEN | F_LENMASK
  jnz   short .next
  push  ECX                      ; save string length
  push  EDI                      ; save string pointer
  repe  cmpsb
  pop   EDI
  pop   ECX
  je    short .found
.next:
  pop   ESI
  or    ESI, ESI
  jnz   short .find
.notfound:                      ; last defined word checked
  jmp   short QUIT              ; quit now and restart interpreter - forth lingo
.found:
  pop   EBX ; discard pointer to next entry
  ; When we get here, SI points to the code of the word, and AL contains
  ; the F_IMMEDIATE flag
STATE equ $+1
startState equ F_STATE_INTERPRET
  or    al, startState   ; or al,1
  xchg  EAX, ESI ; both codepaths need the pointer to be in AX

  ; Execute the word
  mov   ESI, .return
  jz    short compile
  jmp   EAX
.return:
  DWS    InterpreterLoop

;;;;;; Define named and anonymous forth words:

; :[ ColonLBrack
defcode ColonLBrack, "|"
  push  ESI
  call  readWord
  mov   ESI, EDI
  mov   EDI, [HERE]
  ; Creates a dictionary linked list link at DI.
  mov   EAX, EDI
  xchg  [LATEST], EAX  ; AX now points at the old entry, while
                       ; LATEST and DI point at the new one.
  STOSWS
  mov   EAX, ECX                ; prepare len
  stosb                         ; store len
  rep   movsb                   ; store name

  mov   al, 0xe8                ; call op-code
  stosb
  ; The offset is defined as (call target) - (ip after the call instruction)
  ; That works out to DOCOL - (di + WordSize) = DOCOL - WordSize - di
  mov   EAX, DOCOL - WordSize
  sub   EAX, EDI
  mov   [HERE], EDI
  pop   ESI
  jmp   short compile

defcode COMMA, ","
  pop   EAX
  jmp   short compile

; now we define the anonymous words, aka execution tokens (xt):
Semi:
  mov EAX, Exit
  ; fall through to
compile:
HERE equ $+1
  mov   [startHERE],EAX
  add   WSword[HERE],WordSize
  NEXT

Key: ; ( -- c )
%ifdef linux32
  ; read(0, message, 1)
  xor   EAX, EAX
  mov   al, 3                ; system call 3 is read
  ; mov   EAX,3               ; system call 3 is read
  xor   EBX, EBX             ; file handle 0 is stdin
  ;mov   EBX,0                ; file handle 0 is stdin
  push  EBX                  ; push 0
  mov   ECX, ESP             ; read into stack directly
  ;mov   ECX,message          ; address of string to input
  xor   EDX, EDX
  inc   EDX
  ;mov   EDX, 1               ; number of bytes
  int   0x80                 ; invoke operating system to do the read
  NEXT
%endif ; linux32

%ifdef bios16
%ifdef keyboard
  mov   ah, 0                   ; hex: b400
  int   0x16                    ; hex: cd16
  xor   ah, ah
%endif ; keyboard
%ifdef serial
  ; serial input
.serial:
  mov   ah, 2                   ; hex: b402
  xor   dx, dx                  ; hex: 31d2
  int   0x14                    ; hex: cd14
  or    ah, ah                  ; TODO make this error check sense?
  jnz   .serial
%endif ; serial
  jmp   short PushResult
%endif ; bios16

Emit: ; ( c -- )
%ifdef linux32
;  mov   [message],al
  ; write(1, message, 1)
;  mov   EAX,4                ; system call 4 is write
;  mov   EBX,1                ; file handle 1 is stdout
;  mov   ECX,message          ; address of string to output
;  mov   EDX,1                ; number of bytes
  xor   EBX, EBX
  inc   EBX                     ; file handle 1 is stdout
  mov   EDX, EBX                ; number of bytes is 1
  xor   EAX, EAX
  mov   al, 4                ; system call is 4
  mov   ECX, ESP             ; take octet directly from stack
  int   0x80                 ; invoke operating system to do the write
  pop   EAX                  ; adjust stack
  NEXT
%endif ; linux32
%ifdef bios16
  pop   ax
  xor   bx, bx
  mov   ah, 0x0e
  int   0x10
  NEXT
%endif ; bios16


Substract: ; ( n1 n2 -- n1-n2 )
  pop   EAX
%ifdef linux32
  sub   [ESP], EAX
  NEXT
%endif ; linux32
%ifdef bios16
  pop   bx
  sub   ax, bx
  jmp   short PushResult
%endif ; bios16

Nand:   ; ( n1 n2 -- n1 `nand` n2 )
  pop   EBX
  pop   EAX
  and   EAX, EBX
  not   EAX
PushResult:
  push  EAX
  NEXT

Fetch: ; @ ( addr -- val )
  pop   EBX
  push  WSword [EBX]
  NEXT
  ;pop   EAX
  ;mov   EAX, [EAX]
  ;jmp   short PushResult

Store: ; ! ( val addr -- )
  pop   EBX
  pop   WSword [EBX]
  ;mov   [EAX], EBX
  NEXT

QExit: ; ?exit ( 0|x -- )
  pop   EAX
  test  EAX, EAX
  jnz   next
  ; fail trough to
Exit:
  xchg  ESP, EBP
  pop   ESI
  xchg  ESP, EBP
next:
  LODSWS              ; load next word's address into AX
  jmp   EAX           ; jump directly to it

PSPFetch:
  push  ESP
  NEXT

RSPFetch:
  push  EBP
  NEXT

DOCOL:
  xchg  ESP, EBP
  push  ESI
  xchg  ESP, EBP
  pop   ESI ; grab the pointer pushed by `call`
  NEXT

; returns
; DI = pointer to string WAS DX
; CX = string length
; clobbers EAX, EBX, ECX, EDX, EDI
readWord:
%ifdef linux32
  ; read(0, message, 1)
  xor   EBX, EBX             ; file handle 0 is stdin
  mov   ECX, TIB
  mov   EDI, ECX
  xor   EDX, EDX
  inc   EDX                  ; number of bytes
.readLoop:
  xor   EAX, EAX
  mov   al, 3                ; system call 3 is read
;  mov   EAX,3                ; system call 3 is read
;  mov   EBX,0                ; file handle 0 is stdin
;  mov   ECX,message          ; address of string to input
;  mov   EDX, 1               ; number of bytes
  int   0x80                 ; invoke operating system to do the write
  ; clobber EAX
  ; ignore al, i.e. linux errors etc.
  cmp   byte [ECX], ' '
%endif ; linux32

%ifdef bios16
.readLoop:
%ifdef keyboard
  mov   ah, 0                   ; hex: b400
  int   0x16                    ; hex: cd16
%endif ; keyboard
%ifdef serial
  ; serial input
  mov   ah, 2                   ; hex: b402
  xor   dx, dx                  ; hex: 31d2
  int   0x14                    ; hex: cd14
%endif ; serial
  cmp   al, ' '
%endif ; bios16
  jle   .skipOrRet
  inc   ECX
  jmp   short .readLoop
.skipOrRet:
  cmp   ECX, EDI
  je    .readLoop               ; skip initial white space
  sub   ECX, EDI
  ret

startLatest     equ link

%ifdef linux32
%ifdef standalone
%define FillArea 1

startHERE:
  resb  hereSize

SECTION .bss

align   PageSize
TIB:
  resb  PageSize
bufferTop       equ $ - 1

  resb  RSsize
RS0:                            ; start if return stack growing to lower addresses

%endif ; standalone
%endif ; linux32

%ifndef FillArea
TIB     equ     (StartMonitor + 1 + PageSize - 1) & ~ (PageSize - 1)
startHERE equ   TIB + PageSize
RS0     equ     startHERE + hereSize + RSsize
%endif ; FillArea
