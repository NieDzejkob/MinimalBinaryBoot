; Copyright (C) 2021 Dr. Stefan Karrmann
; Distributed unter GPL-3 licence.
; 0x30 or 48 bytes of x86 binary code
; Based on https://niedzejkob.p4.team/bootstrap/miniforth/
; comfort extension
; 1. use first two bytes as size
; 2. add line comments for # and ;
; 3. ignore non octal digits, i.e. [^0-7]


BITS 32
CPU i386

bufferSize      equ 512  ; a boot block - Adjust to taste. Beware of fenceposting.
PageSize        equ 0x1000
SECTION .data
  resb  PageSize * 0x20         ; for stack of loaded code

SECTION .text   write

global _start
_start:
;  we do not use the stack, linux initializes the segment registers
;  push word 0                   ; hex: 6a00
;  pop es                        ; hex: 07
message:                        ; we store the input octet in the initial code.
  std                           ; hex: FD
  mov   edi, buffer + bufferSize - 1 ; hex: bfFE010000 ; Adjust to taste. Beware of fenceposting.
output_loop:
  xor   esi, esi                ; hex: 31F6
  inc   esi                     ; hex: 46
;  mov   esi,1                   ; hex: BB0100 ; collector of input with ready flag
input_loop:                     ; hex:
; mov ah, 0                      ; hex: b400
; int 0x16                       ; hex: cd16

; serial input
;  mov ah,1                      ; hex: b401
;  xor cx,cx                     ; hex: 31c9
;  int 0x14                      ; hex: cd14

  ; read(0, message, 1)
  xor   ebx, ebx                ; hex: 31DB file handle 0 is stdin
  mov   ecx, message            ; hes: B9 92 82 04 08 address of string to input
  mov   edx, ebx                ; hex: 89DA
  inc   edx                     ; hex: 42  1 = number of bytes
  mov   eax, edx                ; hex: 89D0
  inc   eax                     ; hex: 40
  inc   eax                     ; hex: 40   system call 3 is read
  int   0x80                    ; hex: CD80 invoke operating system to do the write
  ; xor   eax,eax               ; we assume return code 1, i.e. one byte read, here
  mov   bl, [message]           ; hex: a0 92 82 04 08

  sub   bl, '0'                 ; hex: 80EB30
  mov   eax, esi                ; hex: 89F0
  shl   eax, 3                  ; hex: C1E003
  or    al,bl                   ; hex: 08D3
  mov   esi,eax                 ; hex: 89D6
  or    ah,ah                   ; hex: 08E4
  ;cmp   ah, 0x00                ; hex: 66 81FBFF00
  jz    short input_loop        ; hex: 74D8
  stosb                         ; hex: aa
  jmp   short output_loop       ; hex: EBD2
  ;jmp   short codeStart         ; hex EBFE , i.e. FE must be the first byte in the loaded code
                                ; as the bytes are reversed, it's actually the last octal code 376
buffer  equ     $ - 1
codeStart:
  resb bufferSize
Xmessage:
  resb 1
reservedToBeUsedByLoadedCode:
  resb 0x1000 * 0x20
