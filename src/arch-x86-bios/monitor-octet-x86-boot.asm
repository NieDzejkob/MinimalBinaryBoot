; 2022 Copyright (C) Dr. Stefan Karrmann
; Distributed unter GPL-3 licence.
; 18 bytes of x86 binary code for Keyboard
; 20 bytes of x86 binary code for uninitialized serial input
; https://niedzejkob.p4.team/bootstrap/miniforth/
; future optional comfort extension:
; 1. use first two bytes as size

%ifdef keyboard
%undef serial
%undef serialInit
%else
%define serial 1
%endif

BITS 16
CPU i386

; Memory layout (hex addresses):
; 0000 - 03ff  IVT, i.e. interrupt vector table
; 0400 - 04ff  Reserved by BIOS
; 0500 - 7bff  unused
; 7c00 -~7c14  MBR (code loaded by BIOS)

bufSize equ 512  ; Adjust to taste. Beware of fenceposting.
; On x86, the boot sector is loaded at 0x7c00 on boot. In segment
; 0x0500, that's 0x7700 (0x0050 << 4 + 0x7700 == 0x7c00).
ORG   0x7700
; Set CS to a known value by performing a far jump. Memory up to
; 0x0500 is used by the BIOS. Setting the segment to 0x0500 gives
; the monitor an entire free segment to work with.
  jmp   0x0050:_start           ; hex: EA05775000
_start:
%ifdef serialInit
   mov ax, 0x00e3        ; ah = 0xe3 = 0b111000111
   ; i.e. bit2 1 and 0: 8 bit word length
   ;      bit 2       : 1 stop bit
   ;      bits 3 and 4: no parity
   ;      bits 5 to 7: baud rate 9600
   xor dx, dx
   int 0x14
%endif ; serialInit
  push cs                ; hex: 0E
  pop  es                ; hex: 07  ; necessary for stosb
  std                    ; hex: fd
  mov di, buffer + bufSize - 1 ; hex: bfXXXX ; Adjust to taste. Beware of fenceposting.
input_loop:              ; hex:
%ifdef keyboard
  mov ah, 0              ; hex: b400
  int 0x16               ; hex: cd16
%endif ; keyboard
%ifdef serial
  mov ah, 1                ; hex: b401
  xor cx, cx               ; hex 31c9
  int 0x14                 ; hex:cd14
%endif ; serial

  stosb                  ; hex: aa
  jmp short input_loop   ; hex: ebf9
  ;jmp short codeStart    ; hex: EB00
buffer   equ   $ - 1
codeStart:
   resb  512 - 2 - ($ - $$)
   db    0x55, 0xAA            ; i.e.: dw 0xAA55 ; little endian
