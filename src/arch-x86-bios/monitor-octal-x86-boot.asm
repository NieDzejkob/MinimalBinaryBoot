; Copyright (C) 2021 Dr. Stefan Karrmann
; Distributed unter GPL-3 licence.
; 0x20 or 32 bytes of x86 binary code for keyboard
; 0x22 or 34 bytes of x86 binary code for unintialized serial input
; 0x29 or 41 bytes of x86 binary code for intialized serial input
; Based on https://niedzejkob.p4.team/bootstrap/miniforth/
; comfort extension if you do not want to be minimal:
; 1. use first two bytes as size
; 2. add line comments for # and ;
; 3. ignore non octal digits, i.e. [^0-7]

%ifdef keyboard
%undef serial
%undef serialInit
%else
%define serial 1
%endif

BITS 16
CPU i386

; Memory layout (hex addresses):
; 0000 - 03ff  IVT, i.e. interrupt vector table
; 0400 - 04ff  Reserved by BIOS
; 0500 - 7bff  unused
; 7c00 -~7c30  MBR (code loaded by BIOS)

bufferSize equ 512  ; a boot block - Adjust to taste. Beware of fenceposting.

SECTION .text                   ; write not necessary
; On x86, the boot sector is loaded at 0x7c00 on boot. In segment
; 0x0500, that's 0x7700 (0x0050 << 4 + 0x7700 == 0x7c00).
ORG   0x7700
; Set CS to a known value by performing a far jump. Memory up to
; 0x0500 is used by the BIOS. Setting the segment to 0x0500 gives
; sectorforth an entire free segment to work with.
  jmp   0x0050:_start           ; hex: EAXXXX5000
_start:
;  we do not use the stack, but this two instructions
  push  cs                      ; hex: 0E
  pop   es                      ; hex: 07 we need es for stosb

%ifdef serialInit
   mov ax, 0x00e3        ; ah = 0xe3 = 0b111000111
   ; i.e. bit2 1 and 0: 8 bit word length
   ;      bit 2       : 1 stop bit
   ;      bits 3 and 4: no parity
   ;      bits 5 to 7: baud rate 9600
   xor dx, dx
   int 0x14
%endif ; serialInit

  std                           ; hex: FD
  mov   di, buffer+bufferSize-1   ; hex: bfXXXX ; Adjust to taste. Beware of fenceposting.
output_loop:
  xor   bx, bx                  ; hex: 31db
  inc   bx                      ; hex: 43
  ;mov   bx, 1                  ; hex: BB0100 ; collector of input with ready flag
  ;mov   bl, 1                   ; hex: B301
input_loop:

%ifdef keyboard
  mov   ah, 0                   ; hex: b400
  int   0x16                    ; hex: cd16
%endif ; keyboard
%ifdef serial
  ; serial input
  mov   ah, 2                   ; hex: b402
  xor   dx, dx                  ; hex: 31d2
  int   0x14                    ; hex: cd14
%endif ; serial

  sub   al, '0'                 ; hex: 2c30
  shl   bx, 3                   ; hex: c1e303
  or    bl,al                   ; hex: 08c3
  or    bh,bh                   ; hex: 08ff
  ;cmp   bh, 0x00                ; hex: 80FF00
  jz    short input_loop        ; hex: 74ed
  stosb                         ; hex: aa
  jmp   short output_loop       ; hex: EBD2
  ;jmp   short codeStart         ; hex EBFE , i.e. FE must be the first byte in the loaded code
                                 ; as the bytes are reversed, it's actually the last octal code 376
buffer   equ   $ - 1
codeStart:
%ifndef noBootMagic
   resb  512 - 2 - ($ - $$)
   db    0x55, 0xAA            ; i.e.: dw 0xAA55 ; little endian
%endif ; noBootMagic
;  resb bufferSize
reservedToBeUsedByLoadedCode:
;  resb 0x1000
