; Register usage:
; SP = parameter stack pointer (grows downwards)
; BP = return stack pointer (grows downwards)
; SI = execution pointer
; TODO test: BX = top of stack
;
; Dictionary structure:
; link: dw
; name: counted string (with flags)
;
; The Forth is DTC, as this saves 2 bytes for each defcode, while costing 3 bytes
; for each defword (of which there are none in the bootsector)
; with bx 0x1A5 bytes

; Memory layout:
; 0000 - 03ff  IVT
; 0400 - 04ff  Reserved by BIOS
; 0500         Start of our segment 0x0050
; 0500 - 05ff  Keyboard input buffer
; 0600 - ...   Return stack (grows upwards)
; ...  -~7c10  Parameter stack
; 7c00 - 7dff  MBR (code loaded by BIOS)
; 7e00 - ...   Dictionary space (HERE / ALLOT)
; .... - ffff  Return stack if in bp

F_IMMEDIATE equ 0x80
F_HIDDEN    equ 0x40
F_LENMASK   equ 0x1f
F_STATE_COMPILE   equ 0
F_STATE_INTERPRET equ 1

TIB     equ     0x0000          ; in segment 0x0050
RS0     equ     0x0600

%ifdef monitorBinary
%undef directBoot
%else
%define directBoot
%endif

%ifdef keyboard
%undef serial
%undef serialInit
%else
%define serial 1
%endif

%define WordSize 2
BITS 16
CPU i386

%define NEXT  jmp short next

%define link 0

; defcode ColonLBrack, "|", F_IMMEDIATE
; defcode Comma, ","
%macro defcode 2-3 0
word_%1:
  dw    link
%define link    word_%1
%strlen %%len %2
    db %3 | %%len, %2
%1:
%endmacro

MBR_Start    equ 0x7700        ; in segment 0x0050

%ifdef directBoot
SECTION .text
StartThis       equ MBR_Start
%ifdef ORG
ORG     StartThis
%endif ; ORG
  jmp   0x0050:start            ; done by monitor but not in directBoot mode
%endif ; directBoot

%ifdef monitorBinary
  SizeOfMonitor equ 32
  StartThis     equ MBR_Start + (SizeOfMonitor - 1)

;SECTION .text   progbits,start=StartThis,align=1
ALIGN 1
SECTION .text   progbits,start=StartThis,align=1,write
ALIGN 1
%ifdef ORG
ORG StartThis
%endif ; ORG

jumpOffset:                       ; from last istruction of the monitor
  ;db    0x00                      ; jump offset of next instruction to _start
  ;jmp   short start              ; 0x32 TODO optimize, i.e. jump directly
  db    0x1A                    ; approximately
%endif

startStack:
  dw    ExitComma               ; Must be top of stack at start !
  dw    Fetch
  dw    Store
  dw    Substr
  dw    Nand
  dw    PSPFetch
  dw    RSPFetch
  dw    Key
  dw    Emit
  dw    STATE
  dw    LATEST
  dw    HERE
;  dw    TIB
;  dw    TOIN

start:
  push  cs
  push  cs
%ifdef directBoot
  push  cs
  pop   es                      ;initialized by monitor
%endif ; directBoot
  pop   ds
  push  ss
  mov   sp, startStack

%ifdef serialInit
   mov ax, 0x00e3        ; ah = 0xe3 = 0b111000111
   ; i.e. bit2 1 and 0: 8 bit word length
   ;      bit 2       : 1 stop bit
   ;      bits 3 and 4: no parity
   ;      bits 5 to 7: baud rate 9600
   xor dx, dx
   int 0x14
%endif ; serialInit
  cld

; stackbottom: ifdef monitorBinary

QUIT:                           ; (re)start the interperter, we do not reset state here
  mov   bp, RS0
InterpreterLoop:
  call  readWord

; Try to find the word in the dictionary.
; SI = dictionary pointer
; DX = string pointer
; CX = string length
LATEST equ $+1
  mov   si, startLatest
.find:
  lodsw
  push  ax ; save pointer to next entry
  lodsb
  xor   al, cl ; if the length matches, then AL contains only the flags
  test  al, F_HIDDEN | F_LENMASK
  jnz   short .next
  mov   di, dx
  push  cx
  repe
  cmpsb
  pop   cx
  je    short .found
.next:
  pop   si
  or    si, si
  jnz   short .find
.notfound:                      ; last defined word checked
  jmp   short QUIT              ; quit now and restart interpreter - forth lingo
.found:
  pop   bx ; discard pointer to next entry
  ; When we get here, SI points to the code of the word, and AL contains
  ; the F_IMMDIATE flag
STATE equ $+1
startState equ F_STATE_INTERPRET
  or    al, startState   ; or al,1
  xchg  ax, si ; both codepaths need the pointer to be in AX

  ; Execute the word
  mov   si, .return
  jz    short compile
  jmp   ax
.return:
  dw    InterpreterLoop

;;;;;; Define named and anonymous forth words:

; :[ ColonLBrack
defcode ColonLBrack, "|"
  push  si
  mov   di, [HERE]
  ; Creates a dictionary linked list link at DI.
  mov   ax, di
  xchg  [LATEST], ax  ; AX now points at the old entry, while
                       ; LATEST and DI point at the new one.
  stosd

  call  readWord
  mov   ax, cx
  stosb                         ; store len
  mov   si, dx
  rep                           ; store name
  movsb

  mov   al, 0xe8 ; call op-code
  stosb

  mov   [HERE], di              ; put di into here
  pop   si
  ; The offset is defined as (call target) - (ip after the call instruction)
  ; That works out to DOCOL - (di + WordSize) = DOCOL - WordSize - di
  mov   ax, DOCOL - WordSize
  sub   ax, di
  jmp   short compile

defcode COMMA, ","
  pop   ax
  jmp   short  compile

; now we define the anonymous words, aka execution tokens (xt):
ExitComma: ; [;] without (1) hidden, (2) switch to interpretation, (3) unhide
  mov ax, Exit
  ;  fall through to
compile:
HERE equ $+1
  mov   [startHERE], ax         ; TODO
  add   word[HERE], WordSize
  NEXT

Key: ; ( -- c )
%ifdef keyboard
  mov   ah, 0                   ; hex: b400
  int   0x16                    ; hex: cd16
  xor   ah, ah
%endif ; keyboard
%ifdef serial
  ; serial input
.serial:
  mov   ah, 2                   ; hex: b402
  xor   dx, dx                  ; hex: 31d2
  int   0x14                    ; hex: cd14
  or    ah, ah
  jnz   .serial
%endif ; serial
  jmp   short PushResult

Emit: ; ( c -- )
  pop   ax
  xor   bx, bx
  mov   ah, 0x0e
  int   0x10
  NEXT  ;bx;

Substr: ; ( n1 n2 -- n1-n2 )
  pop   bx
  pop   ax
  sub   ax, bx
  jmp   short PushResult

Nand:   ; ( n1 n2 -- n1 `nand` n2 )
  pop   bx
  pop   ax
  and   ax,bx
  not   ax
PushResult:
  push  ax
  NEXT

Fetch: ; @ ( addr -- val )
  ;bx;mov   bx, [bx]
  pop   bx
  push  word[bx]
  NEXT

Store: ; ! ( val addr -- )
  ;bx;pop dword [bx]
  pop   bx
  pop   word [bx]
;PopBXNext:
  ;bx;pop bx
  NEXT

QExit: ; ?exit ( 0|x -- )
  pop   bx                     ;bx;
  test  bx,bx
  ;bx; pop   bx
  jnz   next
Exit:  ; exit
  xchg  sp, bp
  pop   si
  xchg  sp, bp
next:
  lodsd               ; load next word's address into AX
  jmp   ax           ; jump directly to it
  ; jmp   [ax]       ; for indirect threaded code (ITC)
  ; TODO Alternativ
  ;mov   si,di
  ;lodsd
  ;mov   si,ax

PSPFetch:
  ;bx;push  bx                ; Contra: bx as TOS ; push sp
  ;bx;mov   bx, sp
  push  sp
  NEXT

RSPFetch:
  ;bx; push  bx                ; Contra: bx as TOS; push di
  ;bx; mov   bx                , di
  push  bp
  NEXT

DOCOL:
  xchg  sp, bp
  push  si
  xchg  sp, bp
  pop   si ; grab the pointer pushed by `call`
  NEXT

; helper functions
; returns
; DX = pointer to string
; CX = string length
; clobbers ax, bx, cx, dx, bp
readWord:
  ; read(0, message, 1)
  xor   bx, bx             ; file handle 0 is stdin
  mov   cx, TIB
  xor   dx, dx
  inc   dx                  ; number of bytes
  mov   bp, cx
.readLoop:
%ifdef keyboard
  mov   ah, 0                   ; hex: b400
  xor   ah, ah                  ;
  int   0x16                    ; hex: cd16
%endif ; keyboard
%ifdef serial
  ; serial input
  mov   ah, 2                   ; hex: b402
  xor   dx, dx                  ; hex: 31d2
  int   0x14                    ; hex: cd14
%endif ; serial
  cmp   al, ' '
  jle   .skipOrRet
  inc   cx
  jmp   short .readLoop
.skipOrRet:
  cmp   cx, bp
  je    .readLoop               ; skip initial white space
  sub   cx, bp
  mov   dx, bp
  ret

;;;;;;;;;;;;;;;;;  end of code ;;;;;;;;;;
startLatest     equ link

PageSize        equ     0x1000 ; 4096 bytes
bufferSize      equ     PageSize
hereSize        equ     16 * PageSize

startHERE:
  ; resb  hersize
