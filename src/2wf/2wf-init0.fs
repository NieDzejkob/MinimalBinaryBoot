| [;] , [;]
| @ , [;]
| ! , [;]
| - , [;]
| nand , [;]
| dsp@ , [;]    \ data stack pointer fetch
\ not necessary for steps 1 to 3, but we take all xt from our stack
| rsp@  , [;]   \ return stack pointer fetch
| key , [;]
| emit , [;]
| ?exit , [;]

\ NB1: | [;] , [;] is a self referencing / recursive interactive definition.
\ We call it here, while it is not yet fully written, but writes itself during the call.
\ NB2: Bar ( | ) could be defined by: : | : [COMPILE] [ [ latest @ reveal ] ;

\ process this file before feeding it initially to 2wf by:
\ sed -e '/^ *$/d' -e 's/  */ /g' -e 's/\\ .*$//' -e 's/( [^)]*)//g' < 2wf.fth

\ Our base are two words: | ( :[ or colonLBracket ) as first word and , ( comma ) .
\ and some well chosen addresses on the parameter stack, i.e. at top:

\ the initial stack is:
( here latestAddr stateAddr ?exit emit key rsp@ dsp@ nand - ! @ [;] )
\ now it is:
( here latestAddr stateAddr )

\ First, we must make [;] immediate. Otherwise, we cannot compile any
\ word using compilation state, as long as we do not have an immediate
\ [ .
\ Second, we need [ i.e. an immediate word that exits compilation state and
\  enters interpretation state. We need it to exit compilation after
\  compiling any word, in particular [ and ] .
\ Third, we want ] i.e. a word to enter compilation state. Otherwise,
\ we must do it manually every time ... In other words, it's syntactic sugar.
\ NB: We cannot define [ at first, since we need compilation state
\ *and* compile exit as its last word. But [;] is not yet immediate
\ and we do not have ' (tick) yet.

\ Let' do it:
\ immediate is ( nt -- ) cell+ dup @ 1 *2 *2 *2 *2 *2 *2 *2 or over ! drop
\ NB: over ! drop is much easier than swap !
\ over to get a copy of latest on top of stack:
  \ dsp@ -cell - @ => dsp@ 0 cell - - @
   dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @
 \ we must follow latest back to [;]
  @ ( nt-?exit ) @ @ @ @ @ @ @ @ @ ( nt-[;] )
 \ cell+ => -cell -
  rsp@ rsp@ - ( 0 ) dsp@ dsp@ - - - ( h l s lenA )
 \ dup => dsp@ @
  dsp@ @ ( h l s lenA lenA )
 @  ( h l s lenA len )
 \ 1 => 0 0 0 nand -
  rsp@ rsp@ - ( 0 ) dsp@ @ dsp@ @ nand -  ( h l s lenA len 1 )
 \ 2* => 0 over - -
  rsp@ rsp@ -  dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @ - -
  rsp@ rsp@ -  dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @ - -
  rsp@ rsp@ -  dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @ - -
  rsp@ rsp@ -  dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @ - -
  rsp@ rsp@ -  dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @ - -
  rsp@ rsp@ -  dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @ - -
  rsp@ rsp@ -  dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @ - -
   ( h l s lenA len 0x80 )
 \ or => invert over invert nand nip
  dsp@ @ nand
  dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @
  dsp@ @ nand
  nand
  \ nip => dsp@ cell+ !
   dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - !
  ( h l s lenA len|0x80 )
 \ over
  dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @   ( h l s lenA len|0x80 lenA )
 !
 \ drop => dup - -
  dsp@ @ - -
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\
\ now [;] is interactive :-)

\ Second step: define ([)
| ([)
 \ IMMEDIATE
  \ i.e.: latest @ cell+ dup @ 1 *2 *2 *2 *2 *2 *2 *2 or over ! drop
  \ over to get a copy of latest on top of stack:
   dsp@  rsp@ rsp@ - dsp@ dsp@ - - - @
   @ ( nt )
   \ cell+ => -cell -
   rsp@ rsp@ - ( 0 ) dsp@ dsp@ - - -
   \ dup => dsp@ @
    dsp@ @ ( st st )
   @
   \ 1 => 0 0 0 nand -
    rsp@ rsp@ - ( 0 ) dsp@ @ dsp@ @ nand -
    \ 2 * => 0 over - -
    rsp@ rsp@ - ( 0 ) dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @ - -
    rsp@ rsp@ - ( 0 ) dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @ - -
    rsp@ rsp@ - ( 0 ) dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @ - -
    rsp@ rsp@ - ( 0 ) dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @ - -
    rsp@ rsp@ - ( 0 ) dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @ - -
    rsp@ rsp@ - ( 0 ) dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @ - -
    rsp@ rsp@ - ( 0 ) dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @ - -
   \ or => invert over invert nand nip
    dsp@ @ nand
    dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @
    dsp@ @ nand
    nand
    \ nip => dsp@ cell+ !
     dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - !
   \ over
    dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @
   !
   \ drop => dup - -
    dsp@ @ - -
  \ now ([) is IMMEDIATE :-)
  \ next: enter compilation mode by
  \ dup dup @ 1 invert and swap !
  \ NB: we cannot replace "swap !" by "over ! drop" here, since drop would be compiled!
   \ dup dup @  ( l s s sv )
    dsp@ @ dsp@ @ @
   \ 0 0 0 nand - ( 1 )
    rsp@ rsp@ - dsp@ @ dsp@ @ nand -
   \ invert and
    dsp@ @ nand nand dsp@ @ nand
   \ swap over over dsp@ cell+ cell+ cell+ ! dsp@ cell+ !
    dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @
    dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @
    dsp@ rsp@ rsp@ - dsp@ dsp@ - - -
        rsp@ rsp@ - dsp@ dsp@ - - -
        rsp@ rsp@ - dsp@ dsp@ - - - !
    dsp@ rsp@ rsp@ - dsp@ dsp@ - - - !
  !
  \ state is 0 now, compile the switch to interpretation state
  \ i.e. dup dup @ 1 or over ! drop
  \ dup dup @ ( l s s sv )
   dsp@ @ dsp@ @ @
  \ 0 0 0 nand - ( 1 )
   rsp@ rsp@ - dsp@ @ dsp@ @ nand -
  \ or => invert over invert nand nip
    \ invert
     dsp@ @ nand
    \ over
     dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @
    \ invert
     dsp@ @ nand
    nand
    \ nip => dsp@ cell+ !
     dsp@  rsp@ rsp@ -  dsp@ dsp@ - - - !
  \  over
   dsp@  rsp@ rsp@ -  dsp@ dsp@ -  - - @
  !
  \ drop => dup - -
   dsp@ @ - -
  [;] \ compile exit it's IMMEDIATE!
([) \ switch back to interpretation, we have defined it just in time
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

\ Third step: compile (])
\ (]) is dup dup @ 1 invert and swap !
\ in compiled code we can simplify this to: dup dup @ 1 invert and over ! drop
| (])
  \ dup dup @  ( l s s sv )
  dsp@ @ dsp@ @ @
  \ 0 0 0 nand - ( 1 )
  rsp@ rsp@ - dsp@ @ dsp@ @ nand -
  \ invert and
  dsp@ @ nand nand dsp@ @ nand
  \ swap over over dsp@ cell+ cell+ cell+ ! dsp@ cell+ !
   dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @
   dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @
   dsp@ rsp@ rsp@ - dsp@ dsp@ - - -
       rsp@ rsp@ - dsp@ dsp@ - - -
       rsp@ rsp@ - dsp@ dsp@ - - - !
   dsp@ rsp@ rsp@ - dsp@ dsp@ - - - !
  !
  \ state is 0 now, compile the same with swap ! replaced by over ! drop :
  \ dup dup @  ( l s s sv )
  dsp@ @ dsp@ @ @
  \ 0 0 0 nand - ( 1 )
  rsp@ rsp@ - dsp@ @ dsp@ @ nand -
  \ invert and
  dsp@ @ nand nand dsp@ @ nand
  \ over ! drop
   dsp@ rsp@ rsp@ - dsp@ dsp@ - - - @
   !
   dsp@ @ - -
  [;] \ compile exit
([) \ switch to interpretation
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

\ Now the hard work is done. Phew!
\ We still have the addresses of the variables ( here latest state )
\ on our stack. We have to manage the state explicitly by ([) and (])
\ and we end a definition with [;].

| dup (]) dsp@ @ [;] ([)
| drop (]) dup - - [;] ([)
| cell (]) dsp@ dsp@ - [;] ([)

| 0 (]) rsp@ rsp@ - [;] ([)       \ | 0 (]) dsp@ dup - [;] ([)
| -cell (]) 0 cell - [;] ([)
| cell+ (]) -cell - [;] ([)

| over (]) dsp@ cell+ @ [;] ([)

\ lit pushes the value on the next cell to the stack at runtime
\ e.g. lit [ 42 , ] pushes 42 to the stack
\ NB: We need an extra cell+ as we have wraped the xt of rsp@ into a
\ normal word.
| lit (]) rsp@ cell+ @ dup cell+ rsp@ cell+ ! @ [;] ([)

\ We would like to do: latest @ cell - @ constant 'exit
\ but constant needs 'exit
| 'exit (]) lit ([) over @ cell - @ , (]) [;] ([)
| constant (]) | lit lit , , 'exit , [;] ([)

\ first redifinitions (i.e. speed sugar). defered to assembler as lit
\ is not faster than the initial code.
\ 0 constant 0
\ cell constant cell
\ -cell constant -cell

| nip (]) ( x y -- y ) dsp@ cell+ ! [;] ([)
| swap (]) over over ( x y x y ) dsp@ cell+ cell+ cell+ ! dsp@ cell+ ! [;] ([)
| negate (]) 0 over - nip [;] ([)
\ | negate (]) 0 swap - [;] ([) \ | negate (]) invert -1 - [;] ([)
| + (]) negate - [;] ([)
| *2 (]) 0 over - - [;] ([) \ | *2 (]) dup + [;] ([)

\ now we can copy the variable addresses in the stack into words.
\ Beware, ([) and (]) still need them on the stack!
dup constant state
over constant latest

| -1 (]) 0 dup nand [;] ([) \ 0 dup nand constant -1  \
|  1 (]) 0 -1 - [;] ([)     \ 0 -1 -     constant  1  \

| invert (]) dup nand [;] ([)
| and (]) nand invert [;] ([)
| or (]) invert over invert nand nip [;] ([)
\ | or (]) invert swap invert nand [;] ([) \ x v y = ~ ( ~x ^ ~y )
\ x xor y <=> (x nand (x nand y)) nand (y nand (x nand y))
\         <=> (x NAND (y NAND y)) NAND ((x NAND x) NAND y)
| xor (])  over over nand \ ( x y !(x&y) )
           swap over nand \ ( x !(x&y) !(y&!(x&y) )
           swap dsp@ cell+ cell+ @ nand nand nip [;] ([)

| immediate ( nt -- )
  (]) cell+ dup @ lit ([) 1 *2 *2 *2 *2 *2 *2 *2 ( 0x80 little endian! ) , (])
  or over ! drop [;] ([)
| IMMEDIATE (]) latest @ immediate [;] ([)
IMMEDIATE \ make IMMEDIATE IMMEDIATE :-)


\ define more normal standard words:
| hide ( nt -- )
  (]) cell+ dup @ lit ([) 1 *2 *2 *2 *2 *2 *2 ( 0x40 little endian! ) , (])
  or over ! drop [;] ([)
| reveal ( nt -- )
  (]) cell+ dup @ lit ([) 1 *2 *2 *2 *2 *2 *2 ( 0x40 little endian! ) invert , (])
  and over ! drop [;] ([)
| ] (]) state @ lit ([) 1 2* 2* 2* 2* 2* 2* 2* 2* 1- invert , (]) and state ! [;] ([)
| [ ] state @ 1 or state ! [;] ([) \ not yet immediate
| EXIT ] 'exit , [;] ([)           \ not yet immediate
\ ; is again a self referencing word. Well we could use [;] ([) to end it, but this is more fun.
| ; ] IMMEDIATE EXIT latest @ reveal [ ;
latest @ @ dup immediate \ set EXIT immediate
             @ immediate \ set [    immediate
| : ] | ] latest @ hide ; \ Beware, [;] does not reveal the word, but ; does.

\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
\ Now we don't need ([) , (]) , | and [;] anymore and we will not use them.
\ Later, in init1, we will hide them, but we cannot do it now. We
\ don't have parse-name find-name yet.

\ We drop state and latest:
drop drop
\ take the rest, i.e. here, from the initial stack:
constant here
\ now we have a pristine, i.e. empty, parameter stack
\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

: allot	( n -- addr )
	here @ swap	( here n )
	here @ + here !	( adds n to HERE, after this, the old value of HERE is still on the stack )
;
: variable cell allot constant ;

: 1-  1 - ;
: 1+ -1 - ;

: ?dup ( 0|x -- 0|x x ) dup dup ?exit drop ; \ first use of ?exit
( 0|x -- -1|0 )
: =0 0 swap ?exit drop -1 ;
: =  - =0 ;
: <> = invert ;

\ tail calls the next xt directly, without exit and DOCOL.
\ Beware, it does not work with code words!
\ As with lit we need an extra cell+ :
: tail rsp@ cell+ @ ( xt_next ) @ ( call DOCOL ) cell+ 1+ ( E8 op-code ) rsp@ cell+ ! ;

\ initially | was immediate. We used lastLL to get its xt. But this is
\ still useful for linked lists.
: lastLL dup @ =0 ?exit @ tail lastLL [ latest @ reveal \ we do not need EXIT because of tail

: >CFA cell+ dup @ lit [ 1 *2 *2 *2 *2 *2 *2 1 - ( lenmask ) , ] and + 1 + ;
: >DFA >CFA cell+ 1 + ; / skip call DOCOL

\ push/pop return stack
\ TODO !
: >rexit ( addr r:addr0 -- r:addr )
    rsp@ cell+ ! ;           \ override return address with original return
                            \ address from >r
: >r ( x -- r:x)
    rsp@ cell+ @             \ get current return address
    swap rsp@ cell+ !        \ replace top of return stack with value
    >rexit ;                \ push new address to return stack
: r> ( r:x -- x )
    rsp@ cell+ cell+ @       \ get value stored in return stack with >r
    rsp@ cell+ @ rsp@ cell+ cell+ !       \ replace value with address to return from r>
    lit 'exit \ [ here @ cell+ cell+ cell+ , ]    \ get address to XXthis word'sXX exit call
    rsp@ cell+ ! ;                 \ make code return to XXthisXX word's exit call,
                            \ effectively calling exit twice to pop return
                            \ stack entry created by >r

\ sed would leave ( without a ) on the same line untouched. We play save:
key (constant '(' \ : '('  lit [ key ( , ] ; \ [ 1 2* 2* 2* 2* 2* 1 2* 2* 2* + , ] \ 40
key )constant ')' \ : ')'  lit [ key ) , ] ; \ [ 1 2* 2* 2* 2* 2* 1 2* 2* 2* + 1 + , ] \ 41
key  constant BL \ : BL   lit [ key   , ] ; \ [ 1 2* 2* 2* 2* 2* , ] \ 32

: c@ ( addr -- c ) @ lit [ 1 *2 *2 *2 *2 *2 *2 *2 *2 1- , ] and ;
\ : c! ( c addr -- ) swap over ( addr c addr )
\     @ lit [ 1 *2 *2 *2 *2 *2 *2 *2 *2 1 - , ] invert and
\     swap lit [ 1 *2 *2 *2 *2 *2 *2 *2 *2 1 - , ] and or swap ! ;
: c! ( c addr -- )
  over lit [ 1 *2 *2 *2 *2 *2 *2 *2 *2 1- , ] and
  over @ lit [ 1 *2 *2 *2 *2 *2 *2 *2 *2 1- , ] invert and
  or over ! drop drop ;
: c, here @ c! here @ 1+ here ! ;

\ recurse makes a recursive call to the current word that is being compiled.

\ Normally while a word is being compiled, it is marked HIDDEN so that references to the
\ same word within are calls to the previous definition of the word.  However we still have
\ access to the word which we are currently compiling through the LATEST pointer so we
\ can use that to compile a recursive call.
: RECURSE IMMEDIATE
  latest @        \ LATEST points to the word being compiled at the moment
  >CFA            \ get the codeword
  ,               \ compile it
;

\ unconditional branch
: branch ( r:addr -- r:addr+offset )
    rsp@ cell+ @ dup @ + rsp@ cell+ ! ;
\   ff4  ff8  c7a ~  4 c7e      ff8
\ conditional branch when top of stack is 0
: ?branch ( r:addr -- r:addr | r:addr+offset)
    0= rsp@ cell+ @ @ cell - and rsp@ cell+ @ + cell+ rsp@ cell+ ! ;

: T( '(' emit ;
: T) ')' emit ;
: T1 branch [ cell , ] T( T) T( T) ;
: T2 T) branch [ 0 , ] T( T) ;
\ : T3 T)  0 ?branch [ cell , ] T( T) ;
\ : T4 T) -1 ?branch [ cell , ] T( T) ;

T(
T)
\ T1
\ T2
\ T3
\ T4

\ ['] is identical to lit, the choice of either depends on context
\ don't write as : ['] lit ; as that will break lit's assumptions about
\ the return stack
: ['] ( -- addr ) rsp@ cell+ @ dup cell+ rsp@ cell+ ! @ ;
\ : ['] tail lit [ latest @ reveal \ no exit
: execute ( xt -- ; executes xt ) >r ;
: r@ r> dup >r ;

: 2drop drop drop ;
: 2dup over over ;
: 2over ( d1 d2 -- d1 d2 d1 ) dsp@ cell+ cell+ dup cell+ @ @ ;
: 2nip ( d1 d2 -- d2 ) dsp@ cell+ cell+ ! dsp@ cell+ cell+ ! ;
: tuck ( x1 x2 -- x2 x1 x2 ) swap over ;
: rot ( x1 x2 x3 -- x2 x3 x1 ) >r swap r> swap ;
: -rot ( x1 x2 x3 -- x3 x1 x2 ) swap >r swap r> ;

\ \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

\ control structures
: IF IMMEDIATE
        lit 0branch ,   \ compile 0branch
        here @          \ save location of the offset on the stack
        0 ,             \ compile a dummy offset
;

: THEN IMMEDIATE
        dup
        here @ swap -   \ calculate the offset from the address saved on the stack
        swap !          \ store the offset in the back-filled location
;

: ELSE IMMEDIATE
        lit branch ,    \ definite branch to just over the false-part
        here @          \ save location of the offset on the stack
        0 ,             \ compile a dummy offset
        swap            \ now back-fill the original (IF) offset
        dup             \ same as for THEN word above
        here @ swap -
        swap !
;

\ BEGIN loop-part condition UNTIL
\ -- compiles to: --> loop-part condition 0BRANCH OFFSET
\ where OFFSET points back to the loop-part
\ This is like do { loop-part } while (condition) in the C language
: BEGIN IMMEDIATE
        here @          \ save location on the stack
;

: UNTIL IMMEDIATE
        lit 0branch ,   \ compile 0BRANCH
        here @ -        \ calculate the offset from the address saved on the stack
        ,               \ compile the offset here
;

\ BEGIN loop-part AGAIN
\ -- compiles to: --> loop-part BRANCH OFFSET
\ where OFFSET points back to the loop-part
\ In other words, an infinite loop which can only be returned from with EXIT
: AGAIN IMMEDIATE
        lit branch ,    \ compile BRANCH
        here @ -        \ calculate the offset back
        ,               \ compile the offset here
;

\ BEGIN condition WHILE loop-part REPEAT
\ -- compiles to: --> condition 0BRANCH OFFSET2 loop-part BRANCH OFFSET
\ where OFFSET points back to condition (the beginning) and OFFSET2 points to after the whole piece of code
\ So this is like a while (condition) { loop-part } loop in the C language
: WHILE IMMEDIATE
        lit 0branch ,   \ compile 0branch
        here @          \ save location of the offset2 on the stack
        0 ,             \ compile a dummy offset2
;

: REPEAT IMMEDIATE
        lit branch ,    \ compile branch
        swap            \ get the original offset (from BEGIN)
        here @ - ,      \ and compile it after BRANCH
        dup
        here @ swap -   \ calculate the offset2
        swap !          \ and back-fill it in the original location
;

\ UNLESS is the same as IF but the test is reversed.

\ Note the use of [COMPILE]: Since IF is IMMEDIATE we don't want it to be executed while UNLESS
\ is compiling, but while UNLESS is running (which happens to be when whatever word using UNLESS is
\ being compiled -- whew!).  So we use [COMPILE] to reverse the effect of marking IF as immediate.
\ This trick is generally used when we want to write our own control words without having to
\ implement them all in terms of the primitives 0BRANCH and BRANCH, but instead reusing simpler
\ control words like (in this instance) IF.

\ TODO: we do not have [COMPILE] yet.
\ : unless IMMEDIATE
\        lit not ,       \ compile NOT (to reverse the test) - todo POSTPONE it
\        [COMPILE] IF    \ continue by calling the normal IF
\ ;
