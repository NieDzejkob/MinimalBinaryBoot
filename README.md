# Minimal Binary Boot

The objective of this project is to provide a minimal binary seed
for booting.

The minor objective is to do it in code that is understandable
by 75% of programmers.

## Credits
Most of the ideas are from other projects. In particular:
- PreForth by Ulrich Hoffmann, c.f. https://github.com/uho/preForth
- SectorForth by Cesar Blum , c.f. https://github.com/cesarblum/sectorforth
- JonesForth by Richard WM Jones, c.f. http://rwmj.wordpress.com/2010/08/07/jonesforth-git-repository/ or https://github.com/nornagon/jonesforth
- MiniForth by Jakub Kądziołka, c.f. https://github.com/NieDzejkob/miniforth
- stage0 by Jeremiah Oriansj with a binary seed for x86 of 357 bytes, c.f. https://github.com/oriansj/stage0/
- Sectorlisp by Justine Tunney with 436 bytes, c.f. https://justine.lol/sectorlisp2/

The most important original new ideas are

1. 2wf, i.e. two word forth, with intense tail code sharing, and
2. the octal monitor with the shifting flag which controls if we read
   the next octal digit or emit  the next octet into the memory.

# State

- I'm still working (i.e. debugging) on the code for the test environment using 32 bit linux. It's now down to 315 bytes for 2wf.
- The monitor sizes are
  - for octal linux input on x86:                48 bytes
  - for octal keyboard input on x86:             32 bytes
  - for octal uninitialized serial input on x86: 34 bytes
  - for octal initialized serial input on x86:   41 bytes

  - for octet keyboard input on x86:             18 bytes (!)
  - for octet uninitialized serial input on x86: 20 bytes
  - for octet initialized serial input on x86:   27 bytes

- The 16 bit bios code of 2wf for keyboad is down to 235 bytes
  standalone and 229 bytes to be loaded by the monitor. Both are not
  yet debugged.

  NB: It less than one half of a boot sector!

Improvements are welcome.

## Notes on Gnu assembler as
I have found no way to compile a short jump without doubt.
Gnu as optimizes jmp on its own and do not provide something like jmpb AFAIK.
Any hints are welcome.

### First solution hack
    .macro jmpb target
      pre_jump_byte_\@ :
        jmp   \target
      .fill   0xffffffff * (. - 2 - pre_jump_byte_\@), 8, 0xdeadbeef
      # its size is zero or to big
    .endm

# Bootstrapping

## Seeding
### Binary seed

The binary seed is only a monitor, i.e. a code loader. The default
one reads octal numbers and writes them as octets into memory.

The last octet overwrites the last jump offset of the monitor. Thus,
we jump into the new code.
##### Why octal?
Octal code is easier to parse and convert to octets. We only have to
substract '0'. With hex code we must substract either '0' or 'A' or
'a'. Therefore, the octal monitor is smaller than a hex monitor.

#### Alt numpad monitor
On x86 hardware we can use a monitor which reads octets, because
of the alt numpad input method. It allows us to enter
octals numbers directly.

### Octal seed or two word forth (2wf)
The monitor input is a minimal forth interpreter compiled to machine code,
all bytes reversed and encoded as octal numbers. It has only two
regular forth words and a well chosen initial stack. The stack contains some
execution tokens (xt) and some variable addresses.

- The purist compiles the assembler to machine code by pen and paper; then he
reverses the bytes, does the octal encoding and enter the result by hand.
- The lazy one uses make, yasm, and python.

2wf itself fits easily into a boot sector of 512 octets (aka bytes).
Thus, it is small by itself but huge in comparison to the octal monitor.

The 2wf reads its input, which defines step by step the normal forth words.

We can debate endlessly if reverse octal code is really source code.
An alternative to reverse octal code is, to use 2wf directly as binary
seed. Then the first input is forth source code without comments. We
will provide both seeds.

#### Is reverse octal code source code?
Well, some accept hex0 as source code. It's hex numbers with
whitespace and line comments. The step to reverse octal code is small,
IMHO. We have to strip the comments, the whitespace. Then we convert
hex numbers to octal numbers. Finally, we reverse the byte sequence.

The conversion from reverse octal code to hex code is analoge. Both
conversions can easily be done manually, by pen and paper, or simple
scripts.

#### 2wf rambling and roadmap
I'm not a forth programmer and it' many years ago, that I read about
forth.

IMHO forth is really a special language. Its machine modell is simple,
so are its inner and outer interpreter. This allows us to write small
forth interpreters.

On the other hand, forth is easily extendable, has control structures,
so that we can write complex programs and abstractions. It's a
fascinating mixture between low level and structured programming.

There are several reasons for this:
1. Forth syntax is really simple, i.e. words separated by white space.
2. Forth is interactive, i.e. it has an interpretation state.
3. Forth compiles algorithms to compact binary code.
4. Moreover, newly compiled words extend the intrepreter and/or the
   compiler.

Therefore, we can start really small and extend the inital system by
forth code until we get some comfort.

2wf is a really small forth interpreter. It uses direct threaded code
(DTC), common tail code sharing, and locations in the op-codes to
store some variables. Well, it's only a maybe minimal forth boot
nucleus or a kind of preForth. But we can extend it by an input of
words (in forth lingo) and get a more and more normal forth
environment.

2wf (two word forth) has only two regular words, i.e.
1. | ( bar ) which behaves as if `: | : POSTPONE [ latest
   @ reveal ;` i.e. colon and lbrack without hiding the word it
   defines. In particular, it does not enter compilation mode but
   stays interpretating.
2. , ( comma ) which writes the top of stack into here and advances here.

Both are not immediate.

That's obviously not enough to create a full forth interpreter on its own.
Therfore, we have some execution tokens (xt) on the initial stack.
First of all, we have [;] , which is a stripped down ; , i.e. it compiles exit.
But [;] does not unhide the word, it does not switch to
interpretation mode, and it is not (yet) immediate.

NB: We define [;] by `| [;] , [;]` i.e. we use it, while it is not yet
completly defined. In other words, it completes its own definition
while avoiding an endless recursive loop.

The next xt are "ordinary" actions `@ ! - nand sp@`. We need them to read and set
memory storage, to get the cell size, 0 and 1.
This set is enough to set the immediate flag for [;] and to compile
([) and set it immediate and compile ([) ; in this sequence.
- We need an immediate [;] to complete the compilation of ([) .
    - We cannot compile other words by , yet, as we cannot get the xt
      of an input word onto the parameter stack. We don't have ' yet.
- As soon as ([) is fully defined, but not earlier, we can leave
  compilation state.
- (]) is just a convenient abbreviation to enter compilation state.

Both, ([) and (]) , need the address of the variable state on the
stack. To set words immediate we need the address of the variable
latest on the stack.

The next xt on the inital stack are `rp@ key emit ?exit`. We need them to define
`lit >r r> tail dup? =0 branch ?branch execute`.
On this base we can define: `latest state immediate hide reveal [ ] : ;`
Now, we can define the control structures and loops,
e.g. `if then else begin again until while repeat case` etc.pp.

Up to this, we need to strip our forth comments by hand or by e.g. sed.
With these control structures and the word key we define the comment
words, i.e. \ and a nested ( , at the start of 2wf-init1.fs

We still have no usual outer interpreter. E.g., it does not interpret
or compile numbers. But now we can build
1. our own outer interpreter
2. a basic, but extendable, assembler in forth.

We use the assembler to redefine some words more efficiently.
That is not really necessary for bootstrapping, but it gives us a
faster forth interpreter.

### Forth seed input
The first forth seed is 2wf-init0.fw. We get it by removing all comments from 2wf-init0.fs.
- Again the purist does it by pen and paper and enters it by hand.
- The lazy one uses make and sed.

It's tricky to get forward without toppling over your boots. We assume
some details about the basic machine:
1. little endian => Easy access to lsb (least significant byte)
2. byte alignment => no aligns in assembler code
3. there are no gaps between words => fetch xt of exit

The second forth seed is 2wf-init1.fs. Now, forth itself can handle the comments in forth
source code .fs.

TODO:

The third forth seed is architecture dependent.
It is for example 2wf-init2-x86-linux.fs.
It defines a simple assembler and redefines many words form init0 and init1 more efficiently.

The fourth forth seed is architecture independent again and completes the basic set of forth words.

## forth programs
On top of the forth interpreter we can define several programs.

### hex0
We define the word hex0, which does hex0 processing, cf. [1].

[1] https://github.com/oriansj/stage0 It's seed takes 357 bytes; i.e. much more than 2wf.

### bin2octal-revers
### bin2octet-revers
### elf-text-writable
### strip-forth-comments
### Scheme interpreter
Check http://www.complang.tuwien.ac.at/~schani/oldstuff/scheme_in_forth.tar.gz
### "Tiny C" compiler or C compiler
Check https://github.com/pzembrod/cc64
https://groups.google.com/g/comp.lang.forth/c/lBYFfVJ1qhc#98fc97704cda1b07

# Linux test bed
We start with a test environment for i386 linux. In this environment it is
easier to develop and debug the code.

# Furter Todos:
1. copy'n'paste forth code to qemu simulation
   1. Learn how to use the serial input in qemu while booting.
2. implement in Forth a hexcode word
3. implement a Forth assembler
4. implement a scheme interpreter

# Minimal forth rambling

Our question is:

> What are the primitives for a minimal forth?

IMHO there is no unique answer. There will be serveral small set which
may be minimal depending on the basic machine.

## Choice in 2wf

1. | a stripped down : ( colon )
2. ,
3. [;] s stripped down ; ( semi )
4. @
5. !
6.  -
7. nand
8. sp@
9. rp@
10. key
11. emit
12. ?exit
13. state
14. latest
15. here

That's 12 primitives and 3 addresses of variables. I have found no way
to get rid of `?exit` without another primitive, e.g. `=0`. We need a
method to collapse all non-zero values to one representation, e.g. -1.

`?exit` is efficient as it fall through to either next or `exit`.

The stripped down colon, i.e. | , could factored into `parse-name create `
... I assume, that that will take more bytes in the binary code.

Other useful words are [ and ] , but we can define them using our
minimal set.

Another useful word is ' . Together with , we can compile words
interactivly. I.e. we don't need compile state. Moreover, `find-name`
is part of ' and of the interpreter. Therfore, we could share the
code. But no code is even shorter.

NB: If [ is immediate at start, all other immediate words are just
syntactic sugar. I.e. you can replace an immediate `FOO` always by
`[ foo ]' where `foo` is the non immediate word of `FOO`. Only
`POSTPONE` and friends would be more difficult, as we have to provide
the immediate flag in another way, e.g. as an parameter.
